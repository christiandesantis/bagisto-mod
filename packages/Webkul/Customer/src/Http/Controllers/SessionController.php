<?php

namespace Webkul\Customer\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Webkul\Customer\Models\Customer;
use Webkul\Customer\Http\Listeners\CustomerEventsHandler;
use Webkul\Customer\Repositories\CustomerRepository;
use Laravel\Socialite\Facades\Socialite;
use Cart;
use Cookie;
use Webkul\Customer\Http\Controllers\RegistrationController;

class SessionController extends Controller
{
    /**
     * CustomerRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $customerRepository;

    /**
     * Create a new Repository instance.
     *
     * @param  \Webkul\Customer\Repositories\CustomerRepository  $customer
     *
     * @return void
     */

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new Repository instance.
     *
     * @return void
    */
    public function __construct(CustomerRepository $customerRepository, RegistrationController $registrationController)
    {
        $this->middleware('customer')->except(['show','create', 'redirectToProvider', 'handleProviderCallback']);
        $this->_config = request('_config');

        $this->customerRepository = $customerRepository;

        $this->registrationController = $registrationController;
        // $subscriber = new CustomerEventsHandler;
        // Event::subscribe($subscriber);
    }

    /**
     * Display the resource.
     *
     * @return \Illuminate\View\View
     */
    public function show()
    {
        if (auth()->guard('customer')->check()) {
            return redirect()->route('customer.profile.index');
        } else {
            return view($this->_config['view']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->validate(request(), [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        if (! auth()->guard('customer')->attempt(request(['email', 'password']))) {
            session()->flash('error', trans('shop::app.customer.login-form.invalid-creds'));

            return redirect()->back();
        }

        if (auth()->guard('customer')->user()->status == 0) {
            auth()->guard('customer')->logout();

            session()->flash('warning', trans('shop::app.customer.login-form.not-activated'));

            return redirect()->back();
        }

        if (auth()->guard('customer')->user()->is_verified == 0) {
            session()->flash('info', trans('shop::app.customer.login-form.verify-first'));

            Cookie::queue(Cookie::make('enable-resend', 'true', 1));

            Cookie::queue(Cookie::make('email-for-resend', request('email'), 1));

            auth()->guard('customer')->logout();

            return redirect()->back();
        }

        //Event passed to prepare cart after login
        Event::dispatch('customer.after.login', request('email'));

        return redirect()->intended(route($this->_config['redirect']));
    }

    /**
     * Login after sign up with socialite.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAfterSocialite($email, $provider_id)
    {

        if (! auth()->guard('customer')->attempt(['provider_id' => $provider_id, 'password' => $provider_id])) {
            session()->flash('error', trans('shop::app.customer.login-form.invalid-creds'));

            return redirect()->back();
        }

        if (auth()->guard('customer')->user()->status == 0) {
            auth()->guard('customer')->logout();

            session()->flash('warning', trans('shop::app.customer.login-form.not-activated'));

            return redirect()->back();
        }

        if (auth()->guard('customer')->user()->is_verified == 0) {
            session()->flash('info', trans('shop::app.customer.login-form.verify-first'));

            Cookie::queue(Cookie::make('enable-resend', 'true', 1));

            Cookie::queue(Cookie::make('email-for-resend', $email, 1));

            auth()->guard('customer')->logout();

            return redirect()->back();
        }

        //Event passed to prepare cart after login
        Event::dispatch('customer.after.login', $email);

        session_start();
            $redirect_url = $_SESSION['url'];
        session_destroy();

        return redirect($redirect_url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        auth()->guard('customer')->logout();

        Event::dispatch('customer.after.logout', $id);

        return redirect()->route($this->_config['redirect']);
    }

   /**
    * Redirect the user to the Google authentication page.
    *
    *@return \Illuminate\Http\Response
    */
    public function redirectToProvider($provider)
    {
        session_start();
        $_SESSION['url'] = $_SERVER['HTTP_REFERER'];
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $customer = $this->customerRepository->findOneWhere(['provider' => $provider, 'provider_id' => $user->id]);

        if ($customer) {
            switch ($provider) {
                case 'google':
                    $this->customerRepository->update([
                        'first_name' => $user['given_name'],
                        'last_name' => $user['family_name'],
                        'picture' => $user['picture']
                    ], $customer['id']);
                break;

                case 'facebook':
                    $fullname_array = explode(" ", $user['name']);
                    $name = $fullname_array[0];
                    $lastname = '';
                    for ($i=1; $i < count($fullname_array); $i++) { 
                        if ($i !== 1) {
                            $lastname = $lastname . ' ';
                        }
                        $lastname = $lastname . $fullname_array[$i];
                    }
                    $this->customerRepository->update([
                        'first_name' => $name,
                        'last_name' => $lastname,
                        'picture' => $user->avatar
                    ], $customer['id']);
                break;
            }
            
            // Proceed to login user
            return $this->createAfterSocialite($customer['email'], $customer['provider_id']);
        }
        if ($taken = $this->customerRepository->findOneWhere(['email' => $user->email])) {

            if ($taken['provider']) $message = 'Email already used to Sign In with ' . $taken['provider']; 
            else $message = 'Email already Signed Up through Sign Up form, please use email and password to Sign In';

            session()->flash('error', $message);
            return redirect()->intended(route('customer.session.index'));
        }
        // Proceed to register user
        return $this->registrationController->createSocialite($user, $provider);
    }
}