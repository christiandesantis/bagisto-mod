@if ($logo = core()->getCurrentChannel()->logo_url)
    <img src="{{ env('APP_LOGO_LIVE_URL') }}" alt="{{ config('app.name') }}" style="height: 40px;"/>
@endif