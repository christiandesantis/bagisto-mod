@component('shop::emails.layouts.master')

    <div>
        <div style="text-align: center;">
            <a href="{{ config('app.url') }}">
                @include ('shop::emails.layouts.logo')
            </a>
        </div>


        <div style="padding: 30px;">
            <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
                <p style="font-weight: bold;font-size: 20px;color: #242424;line-height: 24px;">
                    {{ __('shop::app.mail.customer.registration.dear', ['customer_name' => $data['first_name']. ' ' .$data['last_name']]) }},
                </p>

                <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                    {!! __('shop::app.mail.customer.registration.greeting') !!}
                </p>
            </div>

            <div style="font-size: 16px;color: #5E5E5E;line-height: 30px;margin-bottom: 20px !important;">
                <!-- {{ __('shop::app.mail.customer.registration.summary') }} -->
                @if(!isset($data['provider']))
                    Your account has now been created successfully and you can login using your email address and password credentials.
                @else
                    @if($data['provider'] == 'google')
                        Your account has now been created successfully and you can login using your google account.
                    @else
                        Your account has now been created successfully and you can login using your facebook account.
                    @endif

                @endif
                Upon logging in, you will be able to access other services including reviewing past orders, wishlists and editing your account information.
                <br>
                You've also been automatically subscribed into our newsletter so you can be updated with our latest products and promotions, 
                if you do not wish to be subscribed in our newsletter you'll be provided with an unsubscribe button in the following emails you'll get from us.
            </div>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {{ __('shop::app.mail.customer.registration.thanks') }}
            </p>

            
        </div>
    </div>

@endcomponent