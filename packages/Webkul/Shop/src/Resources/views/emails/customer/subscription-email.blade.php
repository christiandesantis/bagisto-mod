@component('shop::emails.layouts.master')

    <div>
        <div style="text-align: center;">
            <a href="{{ config('app.url') }}">
                @include ('shop::emails.layouts.logo')
            </a>
        </div>

        <div  style="font-size:16px; color:#242424; font-weight:600; margin-top: 60px; margin-bottom: 15px">
            {!! __('shop::app.mail.customer.subscription.greeting') !!}
        </div>

        <div>
            <!-- {!! __('shop::app.mail.customer.subscription.summary') !!} -->
            Thanks for subscribing into Gadgetlan newsletter. You'll eventually get emails about our promotions, newest products, etc... but not too often so we don’t overwhelm your inbox. 
            If you still do not want to receive the latest email marketing news then for sure click the button below.
        </div>

        <div  style="margin-top: 40px; text-align: center">
            <a href="{{ route('shop.unsubscribe', $data['token']) }}" style="font-size: 16px;
            color: #FFFFFF; text-align: center; background: #969393; padding: 10px 100px;text-decoration: none;">
                <!-- {!! __('shop::app.mail.customer.subscription.unsubscribe') !!} -->
                Unsubscribe from newsletter
            </a>
        </div>
    </div>

@endcomponent