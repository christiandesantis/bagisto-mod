{!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}
    <login-header></login-header>
{!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}

<script type="text/x-template" id="login-header-template">
    <div class="dropdown">
        <div id="account">

            <div class="welcome-content pull-right" @click="togglePopup">
                @if(auth('customer')->user() && auth('customer')->user()->picture)
                    <img src="{{ auth('customer')->user()->picture }}" style="width: 24px; height: 24px; border-radius: 50%;" class="align-vertical-top mr-2" alt="avatar">
                @else
                    <i class="material-icons align-vertical-top">perm_identity</i>
                @endif
                <span class="text-center">
                    @guest('customer')
                        {{ __('velocity::app.header.welcome-message', ['customer_name' => trans('velocity::app.header.guest')]) }}!
                    @endguest

                    @auth('customer')
                        {{ __('velocity::app.header.welcome-message', ['customer_name' => auth()->guard('customer')->user()->first_name]) }}
                    @endauth
                </span>
                <span class="select-icon rango-arrow-down"></span>
            </div>
        </div>

        <div class="account-modal sensitive-modal hide mt5">
            <!--Content-->
                @guest('customer')
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header no-border pb0">
                            <label class="fs18 grey">{{ __('shop::app.header.title') }}</label>

                            <button type="button" class="close disable-box-shadow" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="white-text fs20" @click="togglePopup">×</span>
                            </button>
                        </div>

                        <!--Body-->
                        <div class="pl10 fs14">
                            <p>{{ __('shop::app.header.dropdown-text') }}</p>
                        </div>

                        <!--Footer-->
                        <div class="modal-footer text-left">
                            <div class="row justify-space-around">
                                <div class="col-xs-12 col-md-6">
                                    <a href="{{ route('customer.session.index') }}">
                                        <button
                                            type="button"
                                            class="theme-btn btn-block fs14 fw6">

                                            {{ __('shop::app.header.sign-in') }}
                                        </button>
                                    </a>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <a href="{{ route('customer.register.index') }}">
                                        <button
                                            type="button"
                                            class="theme-btn btn-block fs14 fw6">
                                            {{ __('shop::app.header.sign-up') }}
                                        </button>
                                    </a>
                                </div>

                                <div class="col-xs-12 mt-3">
                                    <a href="{{ route('redirect', ['provider' => 'google']) }}">
                                        <button class="theme-btn light btn-block fs14 fw6" style="text-decoration: none;">
                                            <!-- <img src="https://www.gadgetlan.com/svg/32px-google-logo.png" alt="" style="height: 18px; margin-left: -4px; margin-right: 8px"> -->
                                            <i class="fab fa-google mr-3" style="font-size: 18px;"></i>
                                            Sign In with Google
                                        </button>
                                    </a>
                                </div>
                                <div class="col-xs-12 mt-3">
                                    <a href="{{ route('redirect', ['provider' => 'facebook']) }}">
                                    <button class="theme-btn light btn-block fs14 fw6" style="text-decoration: none;">
                                            <!-- <img src="https://www.gadgetlan.com/svg/32px-facebook-logo.png" alt="" style="height: 18px; margin-left: -4px; margin-right: 8px"> -->
                                            <i class="fab fa-facebook-f mr-3" style="font-size: 18px;"></i>
                                            Sign In with Facebook
                                        </button>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                @endguest

                @auth('customer')
                    <div class="modal-content customer-options">
                        <div class="customer-session">
                            <label class="">
                                {{ auth()->guard('customer')->user()->first_name }}
                            </label>
                        </div>

                        <ul type="none">
                            <li>
                                <a href="{{ route('customer.profile.index') }}" class="unset">{{ __('shop::app.header.profile') }}</a>
                            </li>

                            <li>
                                <a href="{{ route('customer.orders.index') }}" class="unset">{{ __('velocity::app.shop.general.orders') }}</a>
                            </li>

                            <li>
                                <a href="{{ route('customer.wishlist.index') }}" class="unset">{{ __('shop::app.header.wishlist') }}</a>
                            </li>

                            <li>
                                <a href="{{ route('velocity.customer.product.compare') }}" class="unset">{{ __('velocity::app.customer.compare.text') }}</a>
                            </li>

                            <li>
                                <a href="{{ route('customer.session.destroy') }}" class="unset">{{ __('shop::app.header.logout') }}</a>
                            </li>
                        </ul>
                    </div>
                @endauth
            <!--/.Content-->
        </div>
    </div>
</script>

@push('scripts')
    <script type="text/javascript">

        Vue.component('login-header', {
            template: '#login-header-template',

            methods: {
                togglePopup: function (event) {
                    let accountModal = this.$el.querySelector('.account-modal');
                    let modal = $('#cart-modal-content')[0];

                    if (modal)
                        modal.classList.add('hide');

                    accountModal.classList.toggle('hide');

                    event.stopPropagation();
                }
            }
        })

    </script>
@endpush

