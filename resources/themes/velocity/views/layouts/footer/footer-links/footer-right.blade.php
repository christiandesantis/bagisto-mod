<div class="col-lg-4 col-md-12 col-sm-12 footer-rt-content">
    <div class="row">
        <div class="mb5 col-12">
            <h3>{{ __('velocity::app.home.payment-methods') }}</h3>
        </div>

        <div class="payment-methods col-12">
            @foreach(\Webkul\Payment\Facades\Payment::getPaymentMethods() as $method)
                <div class="method-sticker">
                    {{ $method['method_title'] }}
                </div>
            @endforeach
        </div>
    </div>

    <!-- <div class="row">
        <div class="mb5 col-12">
            <h3>{{ __('velocity::app.home.shipping-methods') }}</h3>
        </div>

        <div class="shipping-methods col-12">
            @foreach(\Webkul\Shipping\Facades\Shipping::getShippingMethods() as $method)
                <div class="method-sticker">
                    {{ $method['method_title'] }}
                </div>
            @endforeach
        </div>
    </div> -->

    <div class="row">
        <div class="mb5 col-12">
            <h3>Follow Us!</h3>
        </div>

        <div class="shipping-methods col-12">
            
            <a href="{{ env('FACEBOOK_PAGE', 'https://www.facebook.com/') }}" target="_blank"><i class="fab fa-facebook-square mr-4" style="font-size: 28px; color: hsla(0,0%,100%,.83);"></i></a>
            <a href="{{ env('INSTAGRAM_PAGE', 'https://www.instagram.com/') }}" target="_blank"><i class="fab fa-instagram mr-4" style="font-size: 28px; color: hsla(0,0%,100%,.83);"></i></a>
            <!-- <div class="method-sticker">
                <a href="#">Facebook</a>
            </div>
            <div class="method-sticker">
                <a href="#">Instagram</a>
            </div> -->
        </div>
    </div>
</div>